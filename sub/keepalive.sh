#!/bin/bash

request=`cat ./data/requests/keepaliverequest.xml`
sessionToken=`cat ./data/sessionToken`
headers='SOAPAction: "login" User-Agent: Jakarta Commons-HttpClient/3.1 Host: api.betfair.com'
connecttimeout=5

if [ "$sessionToken" = "" ]
then
	echo "keepalive --> cannot get the session token"
	exit 1
fi

request=${request/'<sessionToken>?</sessionToken>'/"<sessionToken>$sessionToken</sessionToken>"}

recived=`curl  -v --connect-timeout "$connecttimeout" -H "$headers" -d "$request"  https://api.betfair.com/global/v3/BFGlobalService`

echo "-------------------------------------------------------------------"

case $recived in
	*'<errorCode xsi:type="n2:APIErrorEnum">OK</errorCode>'*)
		echo "keepalive --> ok"
	;;
	*'<errorCode xsi:type="n2:APIErrorEnum">NO_SESSION</errorCode>'*)
		echo "keepalive --> no session"
		exit 1
	;;
	*)
		echo "keepalive --> unknown API error"
		exit 1
	;;
esac

exit 0
